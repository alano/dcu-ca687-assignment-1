import React, {Component, useState, useEffect} from 'react';
import {Card, Col, Row} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

import DataTable from 'react-data-table-component';
import $ from 'jquery';
import Chart from "react-apexcharts"
import _ from 'lodash'

import GoogleMap from "../../App/components/Maps/GoogleMap";
// import Card from "../../App/components/MainCard";

// require( 'datatables.net-bs4' )( $ );
// import dt from 'datatables.net';
// var dt = require( 'datatables.net' )();
// import Card from "../../App/components/MainCard";
import lineChart3 from '../../App/components/Charts/ApexChart/chart/line-chart-3'
import Select from "react-select";
import Papa from 'papaparse'

window.jQuery = $;
window.$ = $;
global.jQuery = $;


const columns =  [
  {
    name: 'Station',
    selector: 'station',
    sortable: true,
  },
  {
    name: 'Pre-COVID',
    selector: 'pre_covid',
    sortable: true,
  },
  {
    name: 'COVID Aware',
    selector: 'covid_aware',
    sortable: true,
  },
  {
    name: 'First Lockdown',
    selector: 'first_lockdown',
    sortable: true,
  },
  {
    name: 'First Reopening',
    selector: 'first_reopening',
    sortable: true,
  },
  {
    name: 'Second Lockdown',
    selector: 'second_lockdown',
    sortable: true,
  },
  {
    name: 'Second Reopening',
    selector: 'second_reopening',
    sortable: true,
  },
  {
    name: 'Third Lockdown',
    selector: 'third_lockdown',
    sortable: true,
  },
]

// const results = [{
//   "station": 'AAA',
//   "pre_covid": 'BBB',
//   "covid_aware": '1111',
//   "first_lockdown": 'BBB',
//   "first_reopening": '1111',
//   "second_lockdown": 'BBB',
//   "second_reopening": 'BBB',
//   "third_lockdown": '1111',
// }]

async function GetData(filename) {

  const data = await fetchCsv(filename)
  console.log('DATA:', data)
  return data
  // const data = Papa.parse(await fetchCsv());
  var count = 0;
  var stations = []
  // const covid_phases = ['pre_covid', 'covid_aware', 'first_lockdown', 'first_reopening', 'second_lockdown', 'second reopening', 'third_lockdown']
  const pre_covid = []
  // const covid_aware = []
  // const first_lockdown = []
  // const first_reopening = []
  // const second_lockdown = []
  // const second_reopening = []
  // const third_lockdown = []

  console.log(filename)
  var tableParse = []
  Papa.parse(await fetchCsv(filename), {
    worker: true, // Don't bog down the main thread if its a big file
    step: function(result) {
      //console.log('RESULT DATA: ', result.data)

      // let split_row = result.data.toString().split(",")
      let row = {value: result.data[0], label: result.data[1]}

      Object.assign(ElementList, row)
      // console.log('test: ', !stations.includes(result.data[7]))
      // if(stations.includes(result.data[7]) === false) stations.push(result.data[7])
      // if(pre_covid.includes('pre_covid') === false) pre_covid.push(result.data[count])

      count++
    },
    complete: function(results, file) {
      console.log('parsing complete read', count, 'records.');
      console.log('stations final:', ElementList);
      // console.log('pre_covid:', pre_covid);
    }
  });


  // return data;
}

const csvJSON = async (csv) => {
  const lines = csv.toString().split("\n");
  const result = [];
  const headers = lines[0].toString().split(",");

  for(let i=1; i<lines.length; i++){

    const obj = {};
    const currentLine = lines[i].split(",");

    for(let j=0; j<headers.length; j++){
      obj[headers[j]] = currentLine[j];
    }

    result.push(obj);
  }
  return result; //JavaScript object
  // return JSON.stringify(result); //JSON
}

async function fetchCsv(filename, returnType) {

  const response = await fetch('data/' + filename);
  const reader = response.body.getReader();
  const result = await reader.read();
  const decoder = new TextDecoder('utf-8');
  const csv = decoder.decode(result.value);

  if (returnType === 'json') return await csvJSON(csv)
  if (returnType === 'csv') return csv

  // console.log('csv:', csv)
  // return await csvJSON(csv)
}


const SamplePage = props => {

  const [stations, setStations] = useState([])
  const [tableData, setTableData] = useState([])

  let tableDataCsv

  useEffect(() => {
    fetchCsv('stations_list.csv', 'json')
      .then(res => {
        // console.log('res: ', res)
        setStations(res)
      })

    fetchCsv('stations_phase_avg.csv', 'csv')
      .then(res => {
        // console.log('res: ', res)
        tableDataCsv = res
        let count = 0
        let tableParse = []
        let results = {}
        let station_id = ''

        // debugger
        Papa.parse(res, {
          preview: 50,
          worker: true, // Don't bog down the main thread if its a big file
          step: function(result) {
            count === 1 && console.log('RESULT DATA: ', result.data)

            if ( station_id === result.data[0]){
              //!station_id && tableParse.push(results)

              if(result.data.length > 1) {
                // const results = {
                //   "station": result.data[3],
                //   "pre_covid": 0,
                //   "covid_aware": 0,
                //   "first_lockdown": 0,
                //   "first_reopening": 0,
                //   "second_lockdown": 0,
                //   "second_reopening": 0,
                //   "third_lockdown": 0
                // }

                if (result.data[2] === 'covid_aware') results.covid_aware = result.data[2]
                if (result.data[2] === 'pre_covid') results.pre_covid = result.data[2]
                if (result.data[2] === 'first_lockdown') results.first_lockdown = result.data[2]
                if (result.data[2] === 'first_reopening') results.first_reopening = result.data[2]
                if (result.data[2] === 'second_lockdown') results.second_lockdown = result.data[2]
                if (result.data[2] === 'second_reopening') results.second_reopening = result.data[2]
                if (result.data[2] === 'third_lockdown') results.third_lockdown = result.data[2]



                // console.log(`tableParse ${count} -- : `, tableParse)
              }
            } else {


              station_id = result.data[0]

              const results = {
                "station": result.data[3],
                "pre_covid": 0,
                "covid_aware": 0,
                "first_lockdown": 0,
                "first_reopening": 0,
                "second_lockdown": 0,
                "second_reopening": 0,
                "third_lockdown": 0
              }

              tableParse.push(results)
            }



            // if(result.data.length > 1) {
            //   const results = {
            //     "station": result.data[3],
            //     "pre_covid": result.data[4],
            //     "covid_aware": result.data[4],
            //     "first_lockdown": result.data[4],
            //     "first_reopening": result.data[4],
            //     "second_lockdown": result.data[4],
            //     "second_reopening": result.data[4],
            //     "third_lockdown": result.data[4],
            //   }
            //   tableParse.push(results)

              console.log(`tableParse ${count} -- : `, tableParse)
            // }
            // let split_row = result.data.toString().split(",")
            // let row = {value: result.data[0], label: result.data[1]}

            // Object.assign(ElementList, row)
            // console.log('test: ', !stations.includes(result.data[7]))
            // if(stations.includes(result.data[7]) === false) stations.push(result.data[7])
            // if(pre_covid.includes('pre_covid') === false) pre_covid.push(result.data[count])

            count++
          },
          complete: function(results) {
            console.log('parsing complete read', count, 'records.');
            setTableData(tableParse)
            console.log('results:', results?.data);
            // console.log('pre_covid:', pre_covid);
          }
        });
      })
  }, [])


  const mountTable = (results) => {

    for(let x = 0; x < results.length; x++) {

    }
  }


    // const covid_phases = ['Pre COVID', 'covid_aware', 'first_lockdown', 'first_reopening', 'second_lockdown', 'second reopening', 'third_lockdown']
    const covid_phases = [
      {value: '', label: 'All COVID Phases'},
      {value: 'pre_covid', label: 'Pre COVID'},
      {value: 'covid_aware', label: 'COVID Aware'},
      {value: 'first_lockdown', label: 'First Lockdown'},
      {value: 'first_reopening', label: 'First Reopening'},
      {value: 'second_lockdown', label: 'Second Lockdown'},
      {value: 'second_reopening', label: 'Second Reopening'},
      {value: 'third_lockdown', label: 'Third Lockdown'}
    ]

    const data = []
    return (

      <Aux>
        <Row>
          <Col sm={12}>
            <Card>{ console.log('tableData: ', tableData)}
              {/*<Card.Header>*/}
              {/*  <Card.Title as="h5">Total Average of Bikes Available by COVID Phase</Card.Title>*/}
              {/*</Card.Header>*/}
              <Card.Body>
                <DataTable
                  title="Total Average of Bikes Available by COVID Phase"
                  columns={columns}
                  data={tableData}
                  fixedHeader={true}
                  fixedHeaderScrollHeight={'45vh'}
                  dense={true}
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            <Card title='Filter by Station'>
              <Select
                className="basic-single"
                classNamePrefix="select"
                defaultValue={{value: '', label:'Select a Bike Station'}}
                name="color"
                options={stations}
                center={true}
              />
            </Card>
          </Col>
          <Col sm={6}>
            <Card title='Filters by COVID Phase'>
              <Select
                className="basic-single"
                classNamePrefix="select"
                defaultValue={covid_phases[0]}
                name="color"
                options={covid_phases}
              />
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Card>
              {/* <Card.Header>
                                <Card.Title as="h5">Bikes availability x COVID period</Card.Title>
                            </Card.Header>*/}
              {/* <Card.Body>  */}
              <Chart {...lineChart3} />
              {/* </Card.Body> */}
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Card>
              {/* <Card.Header>
                                <Card.Title as="h5">Bikes availability x COVID period</Card.Title>
                            </Card.Header>*/}
              {/* <Card.Body>  */}
              <GoogleMap />
              {/* </Card.Body> */}
            </Card>
          </Col>
        </Row>
      </Aux>
    )
}

export default SamplePage;