export default {
    height: 300,
    type: 'line',
    options:{
        chart: {
            zoom: {
                enabled: false
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [5, 7, 5, 8, 4],
            curve: 'straight',
            dashArray: [0, 8, 5, 5, 6]
        },
        colors: ['#0e9e4a', '#ffa21d', '#ff5252', '#000000', '#FF33F9', '#33DAFF'],
        title: {
            text: 'Percentage of available bikes at stations',
            align: 'left'
        },
        markers: {
            size: 0,

            hover: {
                sizeOffset: 6
            }
        },
        xaxis: {
            categories: ['Pre COVID', 'COVID Aware', 'First Lockdown', 'First Reopening', 'Second Lockdown', 'Second Reopening', 'Third Lockdown'
            ],
        },
        tooltip: {
            y: [{
                title: {
                    formatter: (val) => val + ' (%)'
                }
            }, {
                title: {
                    formatter: (val) =>  val + '(%)'
                }
            } ]
        },
        grid: {
            borderColor: '#f1f1f1',
        }
    },
    series: []
}