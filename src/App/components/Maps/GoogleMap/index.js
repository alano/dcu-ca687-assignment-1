import React from 'react';
// import {Row, Col, Card, Form, InputGroup, Button} from 'react-bootstrap';
import {Row, Col, Card} from 'react-bootstrap';
import {Map, Marker, GoogleApiWrapper, InfoWindow}  from 'google-maps-react';
// import {Map, Marker, GoogleApiWrapper, InfoWindow}  from 'google-maps-react';

import Aux from "../../../../hoc/_Aux";    //../../hoc/_Aux";

// const metaData = (window.localStorage.getItem('stations_phase_avg_lat_long') !== null) ? JSON.parse(window.localStorage.getItem('stations_phase_avg_lat_long')) : ''
// console.log('meta:', metaData)

class GoogleMap extends React.Component {

    state = {
        activeMarker: {},
        selectedPlace: {},
        showingInfoWindow: false,
        position: null,
        metaData: (window.localStorage.getItem('stations_data') !== null) ? JSON.parse(window.localStorage.getItem('stations_data')) : ''
    };

    onMarkerClick = (props, marker) => {

        // console.log('props: ', this.state.selectedPlace.desc)
        this.setState({
            activeMarker: marker,
            selectedPlace: props,
            showingInfoWindow: true,
            preCovid: this.state?.selectedPlace?.desc?.pre_covid || 'Not informed',
            covidAware: this.state?.selectedPlace?.desc?.covid_aware || 'Not informed',
            firstLockdown: this.state?.selectedPlace?.desc?.first_lockdown || 'Not informed',
            firstReopening: this.state?.selectedPlace?.desc?.first_reopening || 'Not informed',
            secondLockdown: this.state?.selectedPlace?.desc?.second_lockdown || 'Not informed',
            secondReopening: this.state?.selectedPlace?.desc?.second_reopening || 'Not informed',
            thirdLockdown: this.state?.selectedPlace?.desc?.third_lockdown || 'Not informed',

        });
    }

    onInfoWindowClose = () =>
        this.setState({
            activeMarker: null,
            showingInfoWindow: false
        });

    onMapClicked = () => {
        if (this.state.showingInfoWindow)
            this.setState({
                activeMarker: null,
                showingInfoWindow: false
            });
    };

    componentDidMount() {
        this.renderAutoComplete();
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps.map) this.renderAutoComplete();
    }

    onSubmit(e) {
        e.preventDefault();
    }

    renderAutoComplete() {
        const { google, map } = this.props;

        // console.log('tabeMaps:', tableData)
        if (!google || !map) return;

        const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
        autocomplete.bindTo('bounds', map);

        autocomplete.addListener('place_changed', () => {
            const place = autocomplete.getPlace();

            if (!place.geometry) return;

            if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
            else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            this.setState({ position: place.geometry.location });
        });
    }

    renderMarkers() {
        // console.log('metadata: ', this.state.metaData)
        return this.state?.metaData.map((location, i) => {

            // console.log('location: ', location)
            return <Marker
              key={ i }
              onClick={this.onMarkerClick}
              title = { location.station }
              position = {{ lat: location.station_latitude , lng: location.station_longitude  }}
              desc = { location }
              // info = { '<span>XXXX</span>'}
              // animation = { this.state.animation[i] }
              name = { location.station || '' }/>
        })
    }

    render() {
        return (
            <Aux>
                <Row>
                    <Col xl={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Dublin Bike Stations - Average number of bicycles available per COVID phase</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <div style={{height: '500px', width: '100%'}}>
                                    {/*{ console.log('mapData: ', this.state.metaData) }*/}

                                    {/*  console.log('row: ', row)*/}
                                    <Map
                                      initialCenter={{
                                          lat: 53.34529445959937,
                                          lng: -6.25482687652527
                                      }}
                                        className="map"
                                        google={this.props.google}
                                        onClick={this.onMapClicked}
                                        zoom={14}>

                                        { this.renderMarkers() }

                                        <InfoWindow
                                          marker={this.state.activeMarker}
                                          onClose={this.onInfoWindowClose}
                                          visible={this.state.showingInfoWindow}>
                                            <div>
                                                <h5>{this.state.selectedPlace.name}</h5>
                                                <table>
                                                    <tr>
                                                        <td>Pre COVID:</td>
                                                        <td>{this.state.preCovid}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>COVID Aware:</td>
                                                        <td>{this.state.covidAware}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>First Lockdown:</td>
                                                        <td>{this.state.firstLockdown}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>First Reopening:</td>
                                                        <td>{this.state.firstReopening}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Second Lockdown:</td>
                                                        <td>{this.state.secondLockdown}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Second Reopening:</td>
                                                        <td>{this.state.secondReopening}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Third Lockdown:</td>
                                                        <td>{this.state.thirdLockdown}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </InfoWindow>
                                    </Map>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCRaVbpCsDye1W7AwUwegWlC7j4wbum1-g'
})(GoogleMap);