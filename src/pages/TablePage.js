import React, {useState, useEffect} from 'react';
import {Card, Col, Row} from 'react-bootstrap';

import Aux from "../hoc/_Aux";
import DataTable from 'react-data-table-component';
import $ from 'jquery';

import {stationsPhaseAvg} from '../helpers/utils'
import  { columns } from '../constants/data'


window.jQuery = $;
window.$ = $;
global.jQuery = $;

const TablePage = props => {

  const [tableData, setTableData] = useState([])

  useEffect(() => {

    if(window.localStorage.getItem('stations_data') !== null)
    setTableData(JSON.parse(window.localStorage.getItem('stations_data')))
  else
    setTableData(stationsPhaseAvg('stations_data.csv'))
  }, [])


    return (

      <Aux>
        <Row>
          <Col sm={12}>
            <Card>
              <Card.Body>
                <DataTable
                  title="Total Average of Bikes Available by COVID Phase"
                  columns={columns()}
                  data={tableData || ''}
                  fixedHeader={true}
                  fixedHeaderScrollHeight={'60vh'}
                  dense={true}
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Aux>
    )
}

export default TablePage;