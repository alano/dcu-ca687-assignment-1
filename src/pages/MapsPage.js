import React, {useState, useEffect} from 'react';
import {Card, Col, Row} from 'react-bootstrap';

import Aux from "../hoc/_Aux";
import $ from 'jquery';

import GoogleMap from "../App/components/Maps/GoogleMap";

import {stationsPhaseAvg} from '../helpers/utils'

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const MapsPage = props => {

  const [setTableData] = useState([])

  // let tableDataCsv

  useEffect(() => {
    if(window.localStorage.getItem('stations_data') !== null)
      setTableData(JSON.parse(window.localStorage.getItem('stations_data')))
    else
      setTableData(stationsPhaseAvg('stations_data.csv'))
  }, [])

  return (

    <Aux>
      <Row>
        <Col sm={12}>
          <Card>
            <GoogleMap  />
          </Card>
        </Col>
      </Row>
    </Aux>
  )
}

export default MapsPage;