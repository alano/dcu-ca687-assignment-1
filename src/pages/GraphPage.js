import React, {useState, useEffect} from 'react';
import {Button, Card, Col, Row} from 'react-bootstrap';

import Aux from "../hoc/_Aux";
import $ from 'jquery';
import Chart from "react-apexcharts"

import {fetchCsv, stationsPhaseAvg} from '../helpers/utils'
// import  { covidPhases } from '../constants/data'

import lineChart3 from '../App/components/Charts/ApexChart/chart/line-chart-3'
import Select from "react-select";

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const GraphPage = props => {

  // console.log('lineChart3 props: ', lineChart3)

  lineChart3.height = 500
  const test = [{ ...lineChart3}]

  const [stations, setStations] = useState([])
  const [tableData, setTableData] = useState([])
  const [selectedOption, setSelectedOption] = useState(null)
  const [graphData, setGraphData] = useState(test)


  // setGraphData(test)
  // console.log('graphData full :', graphData);
  // console.log('test full :', test);

  let handleChange = (selectedOption) => {
    setSelectedOption({ selectedOption });
    // console.log(`Option selected:`, selectedOption[0].value)
    // console.log(`Option selected:`, selectedOption);

  }

  useEffect(() => {
    fetchCsv('stations_list.csv', 'json')
      .then(res => {
        setStations(res)
      })

    if(window.localStorage.getItem('stations_data') !== null)
      setTableData(JSON.parse(window.localStorage.getItem('stations_data')))
    else
      setTableData(stationsPhaseAvg('stations_data.csv'))
  }, [])

  const applyFilters = () => {
    let rowLines = []

    if( selectedOption !== null){
      tableData.map((row, i) => (

        // console.log('tableData:', row)
        selectedOption.selectedOption.map((optionRow, i) => ()
          if(optionRow.value === row.station_id) {

            let preCovidPercentage = Math.round((row.pre_covid * 100) / row.bike_stands_avail,2)
            let covidAwarePercentage = Math.round((row.covid_aware *100) / row.bike_stands_avail)
            let firstLockdownPercentage = Math.round((row.first_lockdown *100) / row.bike_stands_avail)
            let firstReopeningPercentage = Math.round((row.first_reopening *100) / row.bike_stands_avail)
            let secondLockdownPercentage = Math.round((row.second_lockdown *100) / row.bike_stands_avail)
            let secondReopeningPercentage = Math.round((row.second_reopening *100) / row.bike_stands_avail)
            let thirdLockdownPercentage = Math.round((row.third_lockdown *100) / row.bike_stands_avail)

            rowLines.push({name: row.station, data: [preCovidPercentage, covidAwarePercentage, firstLockdownPercentage,
                firstReopeningPercentage, secondLockdownPercentage, secondReopeningPercentage, thirdLockdownPercentage]})
          }
        ))
      ))
    }

    const test2 = [{ ...graphData[0], series: rowLines}]
    // lineChart3.series = rowLines
    setGraphData(test2)
    // console.log('rowLines: ', rowLines)
    // console.log('lineChart3 series updated: ', lineChart3.series)
    //
    // console.log('linechart: ', lineChart3)
    // console.log('graphData:', graphData)
  }
  return (

    <Aux>
      <Row>
        <Col sm={6}>
          <Card title='Filter by Station'>{ graphData.series || ''}
            <Select
              className="basic-multi-select"
              classNamePrefix="select"
              // defaultValue={[colourOptions[2], colourOptions[3]]}
              isMulti
              name="colors"
              onChange={handleChange}
              options={stations}

            />
          </Card>
        </Col>
        <Col sm={6}>
          <Button variant='primary' onClick={applyFilters}>Apply selection</Button>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Card>
            <Chart
              options={lineChart3.options}
              height={500}
              type={'line'}
              series={graphData[0].series}
            />
          </Card>
        </Col>
      </Row>
    </Aux>
  )
}

export default GraphPage;