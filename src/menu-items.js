export default {
    items: [
        {
            id: 'support',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-support',
            children: [
                {
                    id: 'sample-page',
                    title: 'DCU CA-687 - Assignment 1',
                    type: 'item',
                    url: '#',
                    classes: 'nav-item disabled',
                    icon: 'feather icon-home'
                },
                {
                    id: 'graph-page',
                    title: 'Graphs View',
                    type: 'item',
                    url: '/graph-page',
                    classes: 'nav-item',
                    icon: 'feather icon-pie-chart'
                },

                {
                    id: 'table-page',
                    title: 'Table View',
                    type: 'item',
                    url: '/table-page',
                    classes: 'nav-item',
                    icon: 'feather icon-file-text'
                },
                {
                    id: 'maps-page',
                    title: 'Maps View',
                    type: 'item',
                    url: '/maps-page',
                    classes: 'nav-item',
                    icon: 'feather icon-map'
                }
            ]
        }
    ]
}