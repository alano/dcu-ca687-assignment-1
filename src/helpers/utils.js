import Papa from "papaparse";


export async function fetchCsv(filename, returnType) {

  const response = await fetch('data/' + filename);
  const reader = response.body.getReader();
  const result = await reader.read();
  const decoder = new TextDecoder('utf-8');
  const csv = decoder.decode(result.value);

  if (returnType === 'json') return await csvJSON(csv)
  if (returnType === 'csv') return csv
}

export const csvJSON = async (csv) => {
  const lines = csv.toString().split("\n");
  const result = [];
  const headers = lines[0].toString().split(",");

  for(let i=1; i<lines.length; i++){

    const obj = {};
    const currentLine = lines[i].split(",");

    for(let j=0; j<headers.length; j++){
      obj[headers[j]] = currentLine[j];
    }

    result.push(obj);
  }
  return result; //JavaScript object
  // return JSON.stringify(result); //JSON
}


export function stationsPhaseAvg(fileName) {

  // console.log('filename: ', fileName.split('.')[0])
  fileName = fileName.split('.')[0]
  if (window.localStorage.getItem(fileName) === null) {

    // TODO Put this call in helpers file and make it generic
    fetchCsv(`${fileName}.csv`, 'csv')
      .then(res => {
        // let count = 0
        let tableParse = []
        let results
        let station_id = ''

        // debugger
        Papa.parse(res, {
          // preview: 500,
          worker: true, // Don't bog down the main thread if its a big file
          step: function (result) {
            // console.log('RESULT DATA: ', result.data)
            // count === 1 && console.log('RESULT DATA: ', result.data)

            if (station_id === result.data[0]) {

              if (result.data.length > 1) {
                if (result.data[2] === 'pre_covid') results.pre_covid = result.data[4]
                if (result.data[2] === 'covid_aware') {
                  results.covid_aware = result.data[4]
                  // console.log('covid aware: ', result.data[4])
                }
                if (result.data[2] === 'first_lockdown') results.first_lockdown = result.data[4]
                if (result.data[2] === 'first_reopening') results.first_reopening = result.data[4]
                if (result.data[2] === 'second_lockdown') results.second_lockdown = result.data[4]
                if (result.data[2] === 'second_reopening') results.second_reopening = result.data[4]
                if (result.data[2] === 'third_lockdown') results.third_lockdown = result.data[4]
                if (typeof  result.data[5] !== 'undefined') results.station_latitude = result.data[5]
                if (typeof  result.data[6] !== 'undefined') results.station_longitude = result.data[6]
                if (typeof  result.data[7] !== 'undefined') results.bike_stands_avail = result.data[7]
                if (typeof  result.data[8] !== 'undefined') results.avail_bikes = result.data[8]

              }
            } else {
              station_id = result.data[0]
              results = {
                "station_id": result.data[0],
                "station": result.data[3],
                "pre_covid": 0,
                "covid_aware": 0,
                "first_lockdown": 0,
                "first_reopening": 0,
                "second_lockdown": 0,
                "second_reopening": 0,
                "third_lockdown": 0,
                "station_latitude": 0,
                "station_longitude": 0,
                "bike_stands_avail": 0,
                "avail_bikes": 0
              }

              if (result.data[2] === 'pre_covid') results.pre_covid = result.data[4]
              if (result.data[2] === 'covid_aware') {
                results.covid_aware = result.data[4]
                // console.log('covid aware: ', result.data[4])
              }
              if (result.data[2] === 'first_lockdown') results.first_lockdown = result.data[4]
              if (result.data[2] === 'first_reopening') results.first_reopening = result.data[4]
              if (result.data[2] === 'second_lockdown') results.second_lockdown = result.data[4]
              if (result.data[2] === 'second_reopening') results.second_reopening = result.data[4]
              if (result.data[2] === 'third_lockdown') results.third_lockdown = result.data[4]
              if (typeof  result.data[5] !== 'undefined') results.station_latitude = result.data[5]
              if (typeof  result.data[6] !== 'undefined') results.station_longitude = result.data[6]
              if (typeof  result.data[7] !== 'undefined') results.bike_stands_avail = result.data[7]
              if (typeof  result.data[8] !== 'undefined') results.avail_bikes = result.data[8]

              tableParse.push(results)
            }
            count++
          },
          complete: function (results) {
            // console.log('parsing complete read', count, 'records.');

            window.localStorage.setItem(fileName, JSON.stringify(tableParse));
            return tableParse
          }
        });
      })
  } else {
    return JSON.parse(window.localStorage.getItem('stations_phase_avg'))
  }
}