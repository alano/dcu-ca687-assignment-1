export const columns = () => ([
  {
    name: 'Station',
    selector: 'station',
    sortable: true,
  },
  {
    name: 'Pre-COVID',
    selector: 'pre_covid',
    sortable: true,
  },
  {
    name: 'COVID Aware',
    selector: 'covid_aware',
    sortable: true,
  },
  {
    name: 'First Lockdown',
    selector: 'first_lockdown',
    sortable: true,
  },
  {
    name: 'First Reopening',
    selector: 'first_reopening',
    sortable: true,
  },
  {
    name: 'Second Lockdown',
    selector: 'second_lockdown',
    sortable: true,
  },
  {
    name: 'Second Reopening',
    selector: 'second_reopening',
    sortable: true,
  },
  {
    name: 'Third Lockdown',
    selector: 'third_lockdown',
    sortable: true,
  },
])

export const covidPhases = () => ([
  {value: '', label: 'All COVID Phases'},
  {value: 'pre_covid', label: 'Pre COVID'},
  {value: 'covid_aware', label: 'COVID Aware'},
  {value: 'first_lockdown', label: 'First Lockdown'},
  {value: 'first_reopening', label: 'First Reopening'},
  {value: 'second_lockdown', label: 'Second Lockdown'},
  {value: 'second_reopening', label: 'Second Reopening'},
  {value: 'third_lockdown', label: 'Third Lockdown'}
])