import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const GraphPage = React.lazy(() => import('./pages/GraphPage'));
const TablePage = React.lazy(() => import('./pages/TablePage'));
const MapsPage = React.lazy(() => import('./pages/MapsPage'));

const routes = [
    { path: '/graph-page', exact: true, name: 'DCU ', component: GraphPage },
    { path: '/table-page', exact: true, name: 'DCU ', component: TablePage },
    { path: '/maps-page', exact: true, name: 'DCU ', component: MapsPage },
];

export default routes;