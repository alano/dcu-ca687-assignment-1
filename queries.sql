SELECT DISTINCT covid_phase
FROM dublin_bike_data
ORDER BY station_name
LIMIT 10000

SELECT station_id, station_name, last_updated_time, ROUND(AVG(available_bikes_at_station))

--Get average number of bikes in station during 2019
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during intial part of 2020 pre first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time


--Get average number of bikes in station during first reopneing
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second_reopening
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during third lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time
-----
-----Weekday
-----

--Get average number of bikes in station during 2019
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during intial part of 2020 pre first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during first reopneing
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second_reopening
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during third lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time
-----
-----Weekend
-----

--Get average number of bikes in station during 2019
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during intial part of 2020 pre first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during first lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during first reopneing
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during second_reopening
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time

--Get average number of bikes in station during third lockdown
SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time