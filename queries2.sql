SELECT DISTINCT covid_phase
FROM dublin_bike_data
ORDER BY station_name
    LIMIT 10000

SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, available_bikes_at_station
FROM dublin_bike_data dbt
WHERE covid_phase = 'pre_covid'
ORDER BY dbt.station_id, last_updated_time

SELECT DISTINCT dbt.station_id, dbt.station_name,  st.rd AS available_bikes_at_station
FROM (SELECT station_id, covid_phase, ROUND(AVG(available_bikes_at_station)) AS rd
      FROM dublin_bike_data
      GROUP BY station_id, covid_phase) st
         JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
    AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id

--Get average number of bikes in station during 2019
    COPY (SELECT DISTINCT dbt.station_id, dbt.station_name, dbt.last_updated_time::date AS last_updated_time , st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during intial part of 2020 pre first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','


--Get average number of bikes in station during first reopneing
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second_reopening
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during third lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','
-----
-----Weekday
-----

--Get average number of bikes in station during 2019
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during intial part of 2020 pre first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during first reopneing
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second_reopening
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during third lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown' AND day_type = 'weekday'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','
-----
-----Weekend
-----

--Get average number of bikes in station during 2019
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'pre_covid' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during intial part of 2020 pre first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'covid_aware' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during first lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during first reopneing
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'first_reopening' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during second_reopening
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'second_reopening' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','

--Get average number of bikes in station during third lockdown
COPY (SELECT dbt.station_id, dbt.station_name, dbt.last_updated_time, st.rd AS available_bikes_at_station
FROM (SELECT station_id, ROUND(AVG(available_bikes_at_station)) AS rd
			FROM dublin_bike_data
			WHERE covid_phase = 'third_lockdown' AND day_type = 'weekend'
			GROUP BY station_id) st
JOIN dublin_bike_data dbt ON dbt.station_id = st.station_id
			AND st.rd = dbt.available_bikes_at_station
ORDER BY dbt.station_id, dbt.last_updated_time) TO '/tmp/temp_file.csv' DELIMITER ','