/*
 Navicat Premium Data Transfer

 Source Server         : dcu-postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : localhost:5455
 Source Catalog        : dcubikes
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 19/03/2021 00:29:03
*/


-- ----------------------------
-- Table structure for dublin_bike_data
-- ----------------------------
DROP TABLE IF EXISTS "public"."dublin_bike_data";
CREATE TABLE "public"."dublin_bike_data" (
  "id" int4 NOT NULL,
  "station_id" int4,
  "station_name" varchar(255) COLLATE "pg_catalog"."default",
  "last_updated_time" timestamptz(6),
  "hour" int2,
  "month" int2,
  "day" varchar(255) COLLATE "pg_catalog"."default",
  "day_type" varchar(255) COLLATE "pg_catalog"."default",
  "covid_phase" varchar(255) COLLATE "pg_catalog"."default",
  "bike_stands_at_station" int2,
  "available_bike_stands_at_station" int2,
  "available_bikes_at_station" int2,
  "station_status" varchar(255) COLLATE "pg_catalog"."default",
  "station_address" varchar(255) COLLATE "pg_catalog"."default",
  "station_longitude" varchar(255) COLLATE "pg_catalog"."default",
  "station_latitude" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."dublin_bike_data" OWNER TO "postgres";

-- ----------------------------
-- Primary Key structure for table dublin_bike_data
-- ----------------------------
ALTER TABLE "public"."dublin_bike_data" ADD CONSTRAINT "dublin_bike_data_pkey" PRIMARY KEY ("id");
